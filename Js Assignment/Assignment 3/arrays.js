const items = [1, 2, 3, 4, 5, 5];
// // -------------- ----------- 1 ----------- ------------------

// function each(elements, cb) {
//   // Do NOT use forEach to complete this function.
//   // Iterates over a list of elements, yielding each in turn to the `cb` function.
//   // This only needs to work with arrays.
//   // You should also pass the index into `cb` as the second argument
//   // based off http://underscorejs.org/#each
// }
const eachArray = [1, 2, 3, 4, 5, 5];
function each(elements, cb) {
  for (let i = 0; i < elements.length; i++) {
    cb(`${i + 1}:${elements[i]}`);
  }
}
const cb = (res) => {
  console.log(res);
};
// each(eachArray, cb);
// -------------- ----------- 2 ----------- ------------------
// function map(elements, cb) {
//   // Do NOT use .map, to complete this function.
//   // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
//   // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
//   // Return the new array.
// }

function map(elements, cb) {
  const k = elements.filter((list) => list);
  cb(k);
}
const cb = (res) => {
  console.log(res);
};
// map(items, cb);
// -------------- ----------- 3 ----------- ------------------

// function reduce(elements, cb, startingValue) {
// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
// }

function reduce(elements, cb, startingValue) {
  let newArr = 0;
  for (const arr of items) newArr += arr;
  console.log(newArr);
}
// reduce(items);
// -------------- ----------- 4 ----------- ------------------
// function find(elements, cb) {
//   // Do NOT use .includes, to complete this function.
//   // Look through each value in `elements` and pass each element to `cb`.
//   // If `cb` returns `true` then return that element.
//   // Return `undefined` if no elements pass the truth test.
// }
function find(elements, cb) {
  for (let i = 0; i < elements.length; i++) {
    cb(elements[i]);
  }
}

function cb(elem) {
  console.log(elem);
}
// find(items, cb);

// -------------- ----------- 5 ----------- ------------------

// function filter(elements, cb) {
// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
// function filter(elements, cb) {
    function filter(elements, cb) {
        let filterArray = [];
        for (let i = 0; i < elements.length; i++) {
          let k = elements[i];
          if (k) {
            filterArray.push(k);
          }
        }
        return cb(filterArray);
      }
      
      function cb(elem) {
        console.log(elem);
      }
      filter(items, cb);
// filter(items, cb);
// -------------- ----------- 6 ----------- ------------------
// use this to test 'flatten'
// const nestedArray = [1, [2], [3, 4]];
// use this to test 'flatten'
const nestedArray = [1, [2], [[3]], [[[4]]]];
function flatten(elements) {
  let flatarr = [];
  for (let i = 0; i < elements.length; i++) {
    if (Array.isArray(elements[i])) {
      flatarr = flatarr.concat(flatten(elements[i]));
    } else {
      flatarr.push(elements[i]);
    }
  }
  return flatarr;
}
// console.log(flatten(nestedArray));
// -------------------------------------------------
