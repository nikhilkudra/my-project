// function counterFactory() {
//     // Return an object that has two methods called `increment` and `decrement`.
//     // `increment` should increment a counter variable in closure scope and return it.
//     // `decrement` should decrement the counter variable and return it.
//   }

const counterFactory = (function () {
  let Counter = 0;
  const changedValue=(val)=> {
    Counter += val;
  }
  return {
    increment: function () {
      changedValue(+1);
    },
    decrement: function () {
      changedValue(-1);
    },
    value: function () {
      return Counter;
    },
  };
})();
// console.log(counterFactory.value());
// counterFactory.increment();
// counterFactory.increment();
// console.log(counterFactory.value());
// counterFactory.increment();
// counterFactory.increment();
// console.log(counterFactory.value());
// counterFactory.decrement();
// counterFactory.decrement();
// console.log(counterFactory.value());
// counterFactory.decrement();
// counterFactory.decrement();
// console.log(counterFactory.value());

// -------------- ----------- 2 ----------- ------------------
// console.log("sdbck");
// function limitFunctionCallCount(cb, n) {
//Should return a function that invokes `cb`.
//The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
// }
function limitFunctionCallCount(cb, n) {
  if (cb) {
    return function () {
      for (let i = 0; i < n; i++) {
        cb(i);
      }
    };
  }
  return null;
}

function item(i) {
  console.log(i);
}

// limitFunctionCallCount(item, 5)();
// -------------- ----------- 3 ----------- ------------------
// function cacheFunction(cb) {
// Should return a funciton that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.
// }
function cacheFunction(cb) {
  let newObj = {};
  return function (...arr) {
    let n = arr[0];
    if (n in newObj) {
      console.log(n);
      return newObj[n];
    } else {
      console.log(n);
      let result = cb(n);
      newObj[n] = result;
      return result;
    }
  };
}
const cacheObj = cacheFunction((x) => {
  if (x === 0) {
    return 1;
  } else {
    return x * cacheObj(x - 1);
  }
});
// console.log(cacheObj(4));
// console.log(cacheObj(5));

//-------------============------------==================
