

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
// ------  ------------ 1----------------- -----
// function main(obj) {
//   console.log(Object.keys(obj));
//   console.log(Object.values(obj));
// }
// to get keys and values
for (key in testObject) {
  console.log(`${key} = ${testObject[key]}`);
}

// -------------- ----------- 2 ----------- ------------------
for (let i in testObject) {
  if (testObject.hasOwnProperty(i)) {
    // console.log(testObject[i]);
  }
}
//  -------------- -------- 3 ------------- --------------------
// function mapObject(obj, cb) {
// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
// }
function mapObject(val, key) {
  let allprop = [val].reduce((acc, item) => [...Object.keys(item), ...acc], []);
  key(allprop);
}
const data = (res) => {
  console.log(res);
};
// mapObject(testObject, data);

//  -------------- -------- 4 ------------- --------------------

function pairs(obj) {
  const result = Object.keys(obj).map((item) => [item, obj[item]]);
  console.log(result);
}
// pairs({ name: "Bruce Wayne", age: 36, location: "Gotham" });

/* -------------- -------- 5 ------------- --------------------
*/
const reverseObject = function (obj) {
  const newObj = new Object();
  for (const key in obj) {
    newObj[obj[key]] = key;
  }
  return newObj;
};
// console.log(reverseObject(testObject));
//-------------- -------- 6 ------------- --------------------
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

defaults = (obj, defaultProps) =>
  obj[defaultProps] !== undefined
    ? console.log(obj[defaultProps])
    : console.log("undefined");

// defaults(testObject, "name");
