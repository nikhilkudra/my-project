/* 
  Problem 1: Giver Info
  return an array of an Objects properties/keys
*/

const student = { name: "Jonas", age: "12", gender: "M", role: "Receiver" };
const studentKeys = Object.keys(student);
console.log(studentKeys);

/* 
  Problem 2: Giver Info
  using the same object as above.
  return an array of an Objects values
*/
// const student = { name: "Jonas", age: "12", gender: "M", role: "Receiver" };

const studentValues = Object.values(student);
console.log(studentValues);

/* 
  Problem 3: Giver Info
  Add a list of charecters to Jonas' object
  The list should be called 'characters' and it should be an array.
  example:
  { name: 'Jonas', age: '12', gender: 'M', role: 'Receiver', characters: ['Lily', 'Mother', 'Father', 'Caleb', 'Asher']}
  next: select that list and iterate over it using `.forEach` simply log the name of each char in the list;
*/
const Jonas = {
  name: "Jonas",
  age: "12",
  gender: "M",
  role: "Receiver",
  characters: ["Lily", "Mother", "Father", "Caleb", "Asher"],
};

const lists = Jonas.characters.map((item) => item);
console.log(lists);
