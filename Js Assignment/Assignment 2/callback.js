/*
  Callbacks functions are used everywhere in JavaScript.
  Remember that a callback is simply a function that is passed into another function.
  That function is then invoked inside the outer function and does some action.
  Take the following example
*/

function greet(name) {
    alert(`Hello, ${name}`);
  }
  function greetCaller(item) {
    let innername = "nik";
    item(innername);
  }
  greetCaller(greet);

/*
 Problem 2: forEach
  use .forEach to loop over the simpsons list and alert each name passed back to your anonymous callback to the console. 
*/
const simpsons = ["Marge", "Lisa", "Homer", "Bart", "Maggie"];
const k = simpsons.forEach((item, i) => alert(`${i + 1}:-${item}`));
console.log(k);

/*
  Problem 3: every
  create a function called `every` that takes in an array and a callback as it's parameters
  loop over every single item (using a native for loop) and pass each item to the callback
  When you're done, make sure that you finish the code inside of `every` and ensure it works the way it's supposed to
*/

const simpsons = ["Marge", "Lisa", "Homer", "Bart", "Maggie"];
function every(item, ind) {
  for (let i = 0; i < item.length; i++) {
    console.log(`:-${item[i]}`);
  }
}
every(simpsons);
