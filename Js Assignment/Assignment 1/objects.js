// ==== Challenge 1: Writing Objects ====

function interns(id, email, name, gender) {
  this.id = id;
  this.email = email;
  this.name = name;
  this.gender = gender;
}
const Mitzi = new interns(1, "mmelloy0@psu.edu", "Mitzi", "F");
const Kennan = new interns(2, "kdiben1@tinypic.com", "Kennan", "M");
const Keven = new interns(3, "kmummery2@wikimedia.org", "Keven", "M");
const Gannie = new interns(4, "gmartinson3@illinois.edu", "Gannie", "M");
const Antonietta = new interns(5, "adaine5@samsung.com", "Antonietta", "F");

// ==== Challenge 2: Reading Object Data ====

console.log(Mitzi.name);
console.log(Kennan.id);
console.log(Keven.email);
console.log(Gannie.name);
console.log(Antonietta.gender);

// ==== Challenge 3: Object Methods ====

console.log(`Hello, my name is ${Kennan.name}!`);

const Antonietta1 = {
  id: 3,
  name: "Antonietta1",
  email: "adaine5@samsung.com",
  gender: "F",
  multiply: function (x, y) {
    return x * y;
  },
};
console.log(Antonietta1.multiply(3, 4));

const parent = {
  name: "Susan",
  age: 70,
  speak: true,
  Speak: function () {
    return `Hello, my name is ${this.name}!`;
  },
  child: {
    name: "George",
    age: 50,
    speak: true,
    Speak: function () {
      return `Hello, my name is ${this.name}!`;
    },
    grandchild: {
      name: "Sam",
      age: 30,
      Speak: function () {
        return `Hello, my name is ${this.name}!`;
      },
    },
  },
};
console.log(parent.name);
console.log(parent.child.age);
console.log(
  `${parent.child.grandchild.name} && ${parent.child.grandchild.age}`
);
console.log(parent.Speak());
console.log(parent.child.Speak());
console.log(parent.child.grandchild.Speak());
