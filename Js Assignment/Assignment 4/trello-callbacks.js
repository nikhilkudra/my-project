// Task 1 board -> lists -> cards for list qwsa221
// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
// Task 3 board -> lists -> cards for all lists simultaneously

// Get the board details using getBoard, use the board id to get lists from getLists for the list id qwsa221.

// Get board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously

// Get board -> lists -> cards for all lists simultaneously

// =========== ========= ----------------- ------------ ========== ============
// function getBoard(item) {
//   console.log("Fetching board...");
//   return setTimeout(function () {
//     let board = {
//       id: "def453ed",
//       name: "Thanos",
//     };
//     console.log("Received board");
//     item(board);
//   }, 1000);
// }
// const newItem = (res) => {
//   console.log(res.id);
// };
// getBoard(newItem);
//==========--------------------===============------------
function getBoard(callback) {
    console.log("Fetching board...");
    return setTimeout(function () {
      let board = {
        id: "def453ed",
        name: "Thanos",
      };
      console.log("Received board");
      callback(board);
    }, 1000);
  }
  
  function getLists(boardId, callback) {
    console.log(`Fetching lists for board id ${boardId}...`);
    return setTimeout(function () {
      let lists = {
        def453ed: [
          {
            id: "qwsa221",
            name: "Mind",
          },
          {
            id: "jwkh245",
            name: "Space",
          },
          {
            id: "azxs123",
            name: "Soul",
          },
          {
            id: "cffv432",
            name: "Time",
          },
          {
            id: "ghnb768",
            name: "Power",
          },
          {
            id: "isks839",
            name: "Reality",
          },
        ],
      };
      console.log(`Received lists for board id ${boardId}`);
      callback(lists[boardId]);
    }, 1000);
  }
  
  function getCards(listId, callback) {
    console.log(`Fetching cards for list id ${listId}...`);
    return setTimeout(function () {
      let cards = {
        qwsa221: [
          {
            id: "1",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`,
          },
          {
            id: "2",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`,
          },
          {
            id: "3",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`,
          },
        ],
        jwkh245: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "3",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "4",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
        azxs123: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
        cffv432: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
        ghnb768: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
      };
      console.log(`Received cards for list id ${listId}`);
      callback(cards[listId]);
    }, 1000);
  }
  
  //===========================
  // Task 1 board -> lists -> cards for list qwsa221
  // Get the board details using getBoard, use the board id to get lists from getLists for the list id qwsa221.
  
  const task1 = (cards, callback) => {
    getBoard((boardId) => {
      getLists(boardId.id, (lists) => {
        console.log(boardId);
        console.log(boardId.id);
        console.log(lists);
        const cardDataId = lists.filter((elem) => elem.id === cards);
        console.log(cardDataId);
        getCards(cardDataId[0].id, (item) => {
          callback(item);
        });
      });
    });
  };
  const callback = (elem) => {
    console.log(elem);
  };
  // task1("qwsa221", callback);
  //===========================
  //===========-----------==================----------
  // Get board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
  
  // Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
  //===========-----------==================----------
  const task2=(id1, id2, callback)=> {
    getBoard(function (boardId) {
      getLists(boardId.id, function (listsId) {
        let cardsId = listsId.filter(
          (obj) => obj.id == id1 || obj.id == id2
        );
        //  console.log(cardsId)
        cardsId.forEach((obj) =>
          getCards(obj.id,  (card)=> {
            callback(card);
            //console.log(card)
          })
        );
      });
    });
  }
  const dataGet = (card) => {
    console.log(card);
  };
  
  // // task2('qwsa221','jwkh245',dataGet)
  
  //============----------------===================
  
  // Get board -> lists -> cards for all lists simultaneously
  
  // // Task 3 board -> lists -> cards for all lists simultaneously
  // //============----------------===================
  function task3(callback) {
    getBoard(function (boardId) {
      getLists(boardId.id, function (listsId) {
        //console.log(listsId)
        listsId.forEach((obj) =>
          getCards(obj.id, function (card) {
            callback(card);
            // console.log(card)
          })
        );
      });
    });
  }
  const cardList = (card) => {
    console.log(card);
  };
  // task3(cardList);
  