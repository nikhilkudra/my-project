function getBoard() {
  return new Promise((resolve, reject) => {
    console.log("Fetching board...");
    setTimeout(function () {
      let board = {
        id: "def453ed",
        name: "Thanos",
      };
      console.log("Received board");
      resolve(board);
    }, 1000);
  });
}

function getLists(boardId) {
  return new Promise((resolve, reject) => {
    console.log(`Fetching lists for board id ${boardId}...`);
    setTimeout(function () {
      let lists = {
        def453ed: [
          {
            id: "qwsa221",
            name: "Mind",
          },
          {
            id: "jwkh245",
            name: "Space",
          },
          {
            id: "azxs123",
            name: "Soul",
          },
          {
            id: "cffv432",
            name: "Time",
          },
          {
            id: "ghnb768",
            name: "Power",
          },
          {
            id: "isks839",
            name: "Reality",
          },
        ],
      };
      console.log(`Received lists for board id ${boardId}`);
      resolve(lists[boardId]);
    }, 500);
  });
}

function getCards(listId) {
  return new Promise((resolve, reject) => {
    console.log(`Fetching cards for list id ${listId}...`);
    setTimeout(function () {
      let cards = {
        qwsa221: [
          {
            id: "1",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`,
          },
          {
            id: "2",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`,
          },
          {
            id: "3",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`,
          },
        ],
        jwkh245: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "3",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "4",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
        azxs123: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
        cffv432: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
        ghnb768: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`,
          },
        ],
      };
      console.log(`Received cards for list id ${listId}`);
      resolve(cards[listId] || []);
    }, 1500);
  });
}
//=============---------------====       Task 1   =====------------------------========================-----

// Task 1 board -> lists -> cards for list qwsa221
const task1 = () => {
  const data = "qwsa221";
  return getBoard()
    .then((boardId) => getLists(boardId.id))
    .then((listId) => {
      // console.log("listid:", listId);
      let newCard = listId.filter((elem) => elem.id === data);
      // console.log("newCard:", newCard);
      return getCards(newCard[0].id);
    })
    .then((cards) => console.log("cards for list qwsa221:", cards));
};
// task1();

//=============---------------====       Task 2      =====------------------------========================-----
// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously

const task2 = () => {
  const id1 = "qwsa221";
  const id2 = "jwkh245";
  return getBoard()
    .then((boardId) => getLists(boardId.id))
    .then((listId) => {
      const lists = listId.filter((elem) => elem.id === id1 || elem.id === id2);
      lists.forEach((element) =>
        getCards(element.id).then((data) => console.log(data))
      );
    });
};
//   task2();

//=============---------------====       Task 2      =====------------------------========================-----
// Task 3 board -> lists -> cards for all lists simultaneously

const task3 = () => {
  return (
    getBoard()
      // .then((data) => console.log(data))
      .then((data) => getLists(data.id))

      .then((data) => {
        data.forEach((val) =>
          getCards(val.id).then((data) => console.log(data))
        );
      })
  );
};
// task3();
